from tkinter import *
import math


class RGB(object):
    x1, x2, x3 = 255, 255, 0
    score = 0
    def rgb(scoreNum):
        if scoreNum != RGB.score:
        #if scoreNum 1= 0:    #DISCO
            if RGB.x1 == 255 and RGB.x2 == 0 and RGB.x3 != 0:
                RGB.x3 -= 17
            elif RGB.x2 == 255 and RGB.x3 == 0 and RGB.x1 != 0:
                RGB.x1 -= 17
            elif RGB.x1 == 255 and RGB.x3 == 0:
                RGB.x2 += 17
            elif RGB.x1 == 0 and RGB.x3 == 255 and RGB.x2 != 0:
                RGB.x2 -= 17
            elif RGB.x1 == 0 and RGB.x2 == 255:
                RGB.x3 += 17
            elif RGB.x2 == 0 and RGB.x3 == 255:
                RGB.x1 += 17
            RGB.score += 1
            print(RGB.x1,RGB.x2,RGB.x3)

        return(RGB._from_rgb((RGB.x1,RGB.x2,RGB.x3)))

    def _from_rgb(rgb):
        """translates an rgb tuple of int to a tkinter friendly color code
        """
        #print("#%02x%02x%02x" % rgb)
        return("#%02x%02x%02x" % rgb)



