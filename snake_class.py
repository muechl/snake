from tkinter import *
import random
import math
from PIL import Image, ImageTk

class Square(object):
    def __init__(self, x, y, c, color,flag=0):
        if flag == 0:
            self.instance = c.create_rectangle(x, y,
                                           x + 20, y + 20,
                                           fill=color)
        elif flag == 1:
            self.instance = c.create_rectangle(x, y,
                                               x + 20, y + 20,
                                               fill=color,state='hidden')

class Snake(object):
    def __init__(self, square_list, c, vector = (1,0)):
        self.c = c
        self.square_list = square_list

        self.betweenCoords = []

        for k in range(2):
            x1, y1, x2, y2 = c.coords(square_list[k].instance)
            step = 0
            for i in range(10):
                self.betweenCoords.append([x1 + step, y1, x2 + step, y2])
                step += 2
        x1, y1, x2, y2 = c.coords(square_list[-1].instance)
        self.betweenCoords.append([x1, y1, x2, y2])

        self.mapping = {"Down": (0, 1), "Right": (1, 0),
                        "Up": (0, -1), "Left": (-1, 0)}
        self.vector = vector

    def move(self):
        k=0
        for i in range(len(self.square_list)-1):
            sqr = self.square_list[i].instance
            newCoords = self.betweenCoords[k+1]
            k+=10
            self.c.coords(sqr,*newCoords)

        x1, y1, x2, y2 = self.c.coords(self.square_list[-1].instance)
        head = self.square_list[-1].instance
        self.betweenCoords.append([x1 + self.vector[0] * 2, y1 + self.vector[1] * 2, x2 + self.vector[0] * 2,
                      y2 + self.vector[1] * 2])
        self.c.coords(head, x1 + self.vector[0] * 2, y1 + self.vector[1] * 2, x2 + self.vector[0] * 2,
                      y2 + self.vector[1] * 2)
        self.betweenCoords.pop(0)



    def check_collision(self, c):
        x1, y1, x2, y2 = c.coords(self.square_list[-1].instance)
        x1 += self.vector[0] * 2
        y1 += self.vector[1] * 2
        x2 += self.vector[0] * 2
        y2 += self.vector[1] * 2
        for i in range(len(self.square_list)-3):
            b1, z1, b2, z2 = c.coords(self.square_list[i].instance)
            if b1 < x1 < b2 and z1 < y1 < z2:
                return (i)
            elif b1 < x2 < b2 and z1 < y2 < z2:
                return (i)
            elif b1 < x2 < b2 and z1 < y1 < z2:
                return (i)
            elif b1 < x1 < b2 and z1 < y2 < z2:
                return (i)
            elif y1==z1 and y2==z2 and (b1<x1<b2 or b1<x2<b2):
                return(i)
            elif x1==b1 and x2==b2 and (z1<y1<z2 or z1<y2<z2):
                return(i)

        return (-1)

    def add_square(self):
        x1, y1, x2, y2 = self.c.coords(self.square_list[0].instance)
        self.square_list.insert(0,Square(x1,y1,self.c,"white"))
        for i in range(10):
            self.betweenCoords.insert(0, [None, None, None, None])



    def clear(self):
        for i in range(len(self.square_list)):
            self.c.delete(self.square_list[i].instance)
        self.square_list = []
        return

    def change_direction(self,event):
        if event.keysym == 'Right' and self.vector == (-1,0):
            return
        elif event.keysym == 'Left' and self.vector == (1,0):
            return
        elif event.keysym == 'Up' and self.vector == (0,1):
            return
        elif event.keysym == 'Down' and self.vector == (0,-1):
            return
        else:
            self.vector = self.mapping[event.keysym]

class Bomb(object):
    def __init__(self):
        self.bomb_list = []
        self.bomb_images = []

    def num_bombs(self,scoreNum):
        if scoreNum < 3:
            return (40)
        if scoreNum <= 12:
            return (30)
        else:
            return (45-int(math.log(scoreNum,1.2)))


    def delete_bombs(self,c):
        for i in range(len(self.bomb_list)):
            c.delete(self.bomb_list[i].instance)
        for j in range(len(self.bomb_images)):
            c.delete(self.bomb_images[j])
        self.bomb_list = []

    def check_collision(self,c,s):
        x1, y1, x2, y2 = c.coords(s.square_list[-1].instance)
        x1 += s.vector[0]*2
        y1 += s.vector[1]*2
        x2 += s.vector[0]*2
        y2 += s.vector[1]*2
        for i in range(len(self.bomb_list)):
            b1 ,z1 ,b2 ,z2 = c.coords(self.bomb_list[i].instance)
            if b1<x1<b2 and z1<y1<z2:
                return(1)
            elif b1<x2<b2 and z1<y2<z2:
                return(1)
            elif b1<x2<b2 and z1<y1<z2:
                return(1)
            elif b1<x1<b2 and z1<y2<z2:
                return(1)
            elif y1==z1 and y2==z2 and (b1<x1<b2 or b1<x2<b2):
                return(1)
            elif x1==b1 and x2==b2 and (z1<y1<z2 or z1<y2<z2):
                return(1)
        return(0)


    def create_bomb(self, c, s, apple, scoreNum,bomb1):


        for i in range(self.num_bombs(scoreNum)):
            xm = []
            ym = []
            for i in range(39):
                xm.append(i)
            for i in range(2,29):
                ym.append(i)
            x1, y1, x2, y2 = c.coords(s.square_list[-1].instance)
            x11=x1//20
            y11=y1//20
            indexx=xm.index(x11)
            srezx=xm[:(indexx-4)]
            srezx2=xm[(indexx+4):]
            srezx.extend(srezx2)
            indexy=ym.index(y11)
            srezy=ym[:(indexy-4)]
            srezy2=ym[(indexy+4):]
            srezy.extend(srezy2)
            for i in range(len(s.square_list)):
                x1, y1, x2, y2 = c.coords(s.square_list[i].instance)
                if x1//20 in srezx and y1//20 in srezy:
                    indexx1=srezx.index(x1//20)
                    indexy1=srezy.index(y1//20)
                    srezx.pop(indexx1)
                    srezy.pop(indexy1)
            flag = 0
            x = random.choice(srezx) * 20
            y = random.choice(srezy) * 20
            if c.coords(apple) == [x, y, x + 20, y + 20]:
                flag += 1
            if flag == 0:
                self.bomb_list.append(Square(x, y, c, 'black',1))
                bom=c.create_image(x+10,y+10,image=bomb1)
                self.bomb_images.append(bom)
            else:
                self.delete_bombs(c)
                self.create_bomb(c, s, apple, scoreNum,bomb1)
                break

    def load_bombs(self,xybomb_list,c):
        for i in range(len(xybomb_list)):
            self.bomb_list.append(Square(xybomb_list[i][0], xybomb_list[i][1], c, 'black',1))
            bom = c.create_image(xybomb_list[i][0] + 10, xybomb_list[i][1] + 10, image=bomb1)
            self.bomb_images.append(bom)