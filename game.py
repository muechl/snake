import test
import math
import pickle
from snake_class import *
import time
from PIL import Image, ImageTk






def create_apple():
    global apple, c, s ,B, app
    x=20*random.randint(1,38)
    y=20*random.randint(2,28)
    flag = 0
    for i in range(len(s.square_list)):
        x1, y1, x2, y2 = c.coords(s.square_list[i].instance)
        if x==x1 and y==y1:
            flag+=1
    if flag == 0:
        apple = c.create_rectangle(x,y,x+20,y+20,fill="red",state='hidden')
        app = c.create_image(x+10,y+10,image=apple1)
        B = Bomb()
        B.create_bomb(c, s, apple,scoreNum,bomb1)

        # iapple = Image.open("apple.png")
        # apple1 = ImageTk.PhotoImage(iapple)

    else:
        create_apple()


def create_snake():
    square_list = [Square(400,300,c,"black"),Square(420,300,c,"black"),Square(440,300,c,"yellow")]
    return Snake(square_list,c)

def load_snake(snake_coords,vector):
    global c
    square_list=[]
    for i in range(len(snake_coords)):
        if i == len(snake_coords)-1:
            square_list.append(Square(snake_coords[i][0], snake_coords[i][1], c, "yellow"))
        else:
            square_list.append(Square(snake_coords[i][0],snake_coords[i][1],c,"white"))
    return(Snake(square_list,c,vector))




def start_game():
    global s , scoreNum
    s = create_snake()
    create_apple()
    c.bind("<KeyPress>",s.change_direction)
    scoreNum = 0
    colorise()
    c.itemconfigure(score, text='Score: ' + str(scoreNum*25))
    main()



def main():
    global IN_GAME, scoreNum, B, flag
    if IN_GAME == "IN_GAME":
        x1,y1,x2,y2 = s.c.coords(s.square_list[-1].instance)
        head_coords = s.square_list[-1].instance
        x1 += s.vector[0]*2
        y1 += s.vector[1]*2
        x2 += s.vector[0]*2
        y2 += s.vector[1]*2
        c.itemconfigure(restart, state='hidden')
        colorise()

        print((100 - lvlSpeed(scoreNum * 25)),'LVLSPEED')
        if x2>800 or x1<0 or y1<40 or y2>600:
            IN_GAME = "GAME_OVER"
            c.itemconfigure(restart,state = 'normal')
            lose_rating()
        else:
            i = s.check_collision(c)
            #i = -1
            if i != -1:
                IN_GAME = "GAME_OVER"
                c.itemconfigure(s.square_list[i].instance, fill = "red")
                c.itemconfigure(restart, state='normal')
                lose_rating()
            else:
                if B.check_collision(c,s) == 1:
                    IN_GAME = "GAME_OVER"
                    c.itemconfigure(s.square_list[-1].instance, fill="red")
                    c.itemconfigure(restart, state='normal')
                    lose_rating()
                else:
                    s.move()
                    head_coords = c.coords(s.square_list[-1].instance)
                    x1, y1, x2, y2 = head_coords
                    if check_collision_apple(c,apple) == 1:
                        s.add_square()
                        c.delete(apple)
                        c.delete(app)
                        B.delete_bombs(c)
                        scoreNum += 1
                        c.itemconfigure(score, text='Score: ' + str(scoreNum*25))
                        create_apple()
        root.after(100 - lvlSpeed(scoreNum * 25), main)
    elif IN_GAME == "PAUSE":
        root.after(0,main)


def check_collision_apple(c,apple):
    x1, y1, x2, y2 = c.coords(s.square_list[-1].instance)
    x1 += s.vector[0] * 10
    y1 += s.vector[1] * 10
    x2 += s.vector[0] * 10
    y2 += s.vector[1] * 10
    b1, z1, b2, z2 = c.coords(apple)
    if b1 < x1 < b2 and z1 < y1 < z2:
        return (1)
    elif b1 < x2 < b2 and z1 < y2 < z2:
        return (1)
    elif b1 < x2 < b2 and z1 < y1 < z2:
        return (1)
    elif b1 < x1 < b2 and z1 < y2 < z2:
        return (1)
    elif y1 == z1 and y2 == z2 and (b1 < x1 < b2 or b1 < x2 < b2):
        return (1)
    elif x1 == b1 and x2 == b2 and (z1 < y1 < z2 or z1 < y2 < z2):
        return (1)
    return (0)

def lvlSpeed(scoreNum):
    if scoreNum < 10:
        return(90)
    if scoreNum <= 300:
        return(scoreNum)
    else:
        return(int(math.log(scoreNum,1.2)))


def colorise():
    c.config(c,bg= test.RGB.rgb(scoreNum))



#def choose_color():


def pause(event):
    global IN_GAME , save, load , bestScores
    if IN_GAME == "PAUSE":
        IN_GAME = "IN_GAME"
        delete_menu()
    elif IN_GAME == "IN_GAME":
        IN_GAME = "PAUSE"
        create_menu()




def delete_menu():
    global save, load, bestScores, menu
    c.delete(save)
    c.delete(load)
    c.delete(bestScores)
    c.delete(menu)

def create_menu():
    global save, load, bestScores, menu
    #menu = c.create_oval(250,250,550,450,fill='red')
    menu = c.create_image(400,350,image=menu1)
    bestScores = c.create_text(400, 400, text='Best scores', font='Times 30', fill='black', state='normal')
    save = c.create_text(400, 300, text='Save game', font='Times 30', fill='black', state='normal')
    load = c.create_text(400, 350, text='Load game', font='Times 30', fill='black', state='normal')
    c.tag_bind(bestScores, "<Button-1>", best_scores)
    c.tag_bind(save, "<Button-1>", save_game)
    c.tag_bind(load, "<Button-1>", load_game)
    c.update_idletasks()

def egg(DK):
    root.after(0,main)
    #Grid.forget(c)
    #d = Canvas(root, width=1800, height=1620, bg="black")
    #d.grid()
    #d.create_text(400, 300, text='EASTER EGG', font='Times 42', fill='white',state = 'normal')
    return

def clicked(restart):
    global IN_GAME , scoreNum
    IN_GAME = "IN_GAME"
    c.itemconfigure(restart, state = 'hidden')
    s.clear()
    c.delete(apple)
    c.delete(app)
    B.delete_bombs(c)
    scoreNum = 0
    test.RGB.score = 0
    test.RGB.x1, test.RGB.x2, test.RGB.x3 = 255, 255, 0
    colorise()
    start_game()
    return()



def leave(event):
    root.destroy()
    return

def save_game(event):
    global scoreNum, s, apple, c
    x1,y1,x2,y2 = c.coords(apple)
    snake_coords = []
    vector = s.vector
    xybomb_list=[]
    for i in range(len(B.bomb_list)):
        xb,yb,xb1,yb1 = (c.coords(B.bomb_list[i].instance))
        xybomb_list.append([xb,yb])

    for i in range(len(s.square_list)):
         snake_coords.append(c.coords(s.square_list[i].instance)[:2])
    dataMap = {'score':scoreNum,'apple_x':x1,'apple_y':y1,'snake_coords':snake_coords,'vector':vector,
               'RGB.score':test.RGB.score,'bombs':xybomb_list,'RGBx1':test.RGB.x1,'RGBx2':test.RGB.x2,'RGBx3':test.RGB.x3,
               'between_coords':s.betweenCoords}
    print(len(s.betweenCoords),'BC')
    pickle.dump(dataMap,open('save 1','wb'),protocol=pickle.HIGHEST_PROTOCOL)
    saved = c.create_text(400,550,text='Saved succesfully',font='Gutsuh 25',fill='white')
    c.update_idletasks()
    time.sleep(1)
    c.itemconfigure(saved,state='hidden')
    c.update_idletasks()
    print('saved succesfully')



def load_game(event):
    global scoreNum, s, apple, c, B, app
    c.delete(apple)
    B.delete_bombs(c)
    for i in range(len(s.square_list)):
        c.delete(s.square_list[i].instance)
    dataMap = pickle.load(open('save 1','rb'))
    x1 = dataMap.get('apple_x')
    y1 = dataMap.get('apple_y')
    snake_coords = dataMap.get('snake_coords')
    scoreNum = dataMap.get('score')
    vector = dataMap.get('vector')
    test.RGB.score = dataMap.get('RGB.score')
    test.RGB.x1 = dataMap.get('RGBx1')
    test.RGB.x2 = dataMap.get('RGBx2')
    test.RGB.x3 = dataMap.get('RGBx3')
    xybomb_list = dataMap.get('bombs')
    s = load_snake(snake_coords,vector)
    s.betweenCoords = dataMap.get('between_coords')
    print(len(s.betweenCoords),'BC1')
    B.load_bombs(xybomb_list,c)
    apple = c.create_oval(x1,y1,x1+20,y1+20,fill='red',state='hidden')
    c.delete(app)
    app = c.create_image(x1 + 10, y1 + 10, image=apple1)
    c.itemconfigure(score, text='Score: ' + str(scoreNum*25))
    c.bind("<KeyPress>", s.change_direction)
    colorise()
    delete_menu()
    create_menu()
    c.update_idletasks()
    colorise()
    print('load succesfuly')
    print('score='+str(test.RGB.score))


def lose_rating():
    global scoreNum
    rating = None
    try:
        rating = pickle.load(open('rating', 'rb'))
        print('rating',rating)
        print('score',scoreNum)
    except FileNotFoundError:
        pass
    if rating == None:
        rating = []
        rating.append(scoreNum*25)
        rating.sort()
    elif len(rating) < 5:
        rating.append(scoreNum*25)
        rating.sort()
    else:
        for i in range(5):
            if scoreNum*25 > rating[i]:
                flag = i
                print('flag',flag)
                rating.pop(0)
                rating.insert(flag,scoreNum*25)
                rating.sort()
                new_record = c.create_text(400,400,text="You set a new record!" + "\n" + "Your score:" + str(scoreNum*25),font='Times 25', fill='black')
                c.update_idletasks()
                time.sleep(3)
                c.itemconfigure(new_record, state='hidden')
                c.update_idletasks()
                break
    print('game_rating', rating)
    print('score1', scoreNum)
    pickle.dump(rating, open('rating', 'wb'), protocol=pickle.HIGHEST_PROTOCOL)


def unb1nd():
    c.tag_unbind(DK, "<Button-1>")
    c.tag_unbind(save, "<Button-1>")
    c.tag_unbind(load, "<Button-1>")
    c.tag_unbind(bestScores, "<Button-1>")
    c.unbind("<Escape>")
    c.tag_unbind(restart, '<Button-1>')
    c.unbind("<KeyPress>")
    c.tag_bind(back, "<Button-1>", go_back)



def b1nd():
    c.tag_bind(DK, "<Button-1>", egg)
    c.tag_bind(save, "<Button-1>", save_game)
    c.tag_bind(load, "<Button-1>", load_game)
    c.tag_bind(bestScores, "<Button-1>", best_scores)
    c.bind("<space>", leave)
    c.bind("<Escape>", pause)
    c.tag_bind(restart, '<Button-1>', clicked)
    c.bind("<KeyPress>", s.change_direction)



def hide_snake(event):
    for i in range(len(s.square_list)):
        c.itemconfigure(s.square_list[i].instance, state = 'hidden')

def appear_snake(event):
    for i in range(len(s.square_list)):
        c.itemconfigure(s.square_list[i].instance, state = 'normal')

def hide_apple(event):
    c.itemconfigure(apple, state='hidden')

def appear_apple(event):
    c.itemconfigure(apple, state='normal')

def best_scores(event):
    global back, background, game_rating, besT, s, c
    c.itemconfigure(background, state='normal')
    c.itemconfigure(back, state = 'normal')
    c.itemconfigure(redline, state='hidden')
    rating = pickle.load(open('rating', 'rb'))
    besT = c.create_text(400, 250, text= 'Best scores:', font='Times 25', fill='white')
    game_rating = c.create_text(400, 300, text= rating, font='Times 25', fill='black')
    unb1nd()
    c.itemconfigure(apple,state = 'hidden')
    for i in range(len(s.square_list)):
        c.itemconfigure(s.square_list[i].instance, state = 'hidden')
    c.update_idletasks()



def go_back(event):
    global besT
    c.itemconfigure(background, state='hidden')
    c.itemconfigure(back, state='hidden')
    c.itemconfigure(redline, state='normal')
    c.delete(game_rating)
    c.delete(besT)
    b1nd()
    c.itemconfigure(apple,state = 'normal')
    for i in range(len(s.square_list)):
        c.itemconfigure(s.square_list[i].instance, state = 'normal')
    c.update_idletasks()


IN_GAME = "IN_GAME"
global score
scoreNum = 0
root = Tk()
try:
    iapple = Image.open("apple.png")
    apple1 = ImageTk.PhotoImage(iapple)

except IOError as e:
    print(e)
    sys.exit(1)
try:
    ibomb = Image.open("bomb.png")
    bomb1 = ImageTk.PhotoImage(ibomb)

except IOError as e:
    print(e)
    sys.exit(1)

try:
    imenu = Image.open("menu.png")
    menu1 = ImageTk.PhotoImage(imenu)

except IOError as e:
    print(e)
    sys.exit(1)


#root.overrideredirect(1) #Убирает элементы окна виндовс
root.title("Snake Game")
w = root.winfo_screenwidth()
h = root.winfo_screenheight()
w = w//2 # середина экрана
h = h//2
w = w - 400 # смещение от середины
h = h - 300
root.geometry('800x600+{}+{}'.format(w, h))
c = Canvas(root, width=800, height=600, bg="#6CBB3C")
c.pack()
c.focus_set() #нажатия
score = c.create_text(400,20,text='Score:'+ str(scoreNum*25),font='Arial 20',fill='black')
DK = c.create_text(100,20,text='Author : DK ',font='Times 20',fill='black')
save = c.create_text(400,300,text='Save game' ,font='Times 30',fill='black',state = 'hidden',)
load = c.create_text(400,350,text='Load game' ,font='Times 30',fill='black',state = 'hidden')


restart = c.create_text(400, 300, text='RESTART', font='Times 20', fill='black',state = 'hidden')
background = c.create_rectangle(0,0,800,600,fill="purple",state = 'hidden')
back = c.create_text(80, 40, text='Go back', font='Times 30', fill='Black',state = 'hidden')
redline = c.create_line(0,40,1300,40,fill='red')
#quit = c.create_text(666,20,text='Quit',font='Arial 20',fill='#666666')
c.tag_bind(back,"<Button-1>",go_back)
c.tag_bind(DK,"<Button-1>",egg)
c.tag_bind(save,"<Button-1>",save_game)
c.tag_bind(load,"<Button-1>",load_game)
c.bind("<space>",leave)
c.bind("<Escape>",pause)
c.tag_bind(restart,'<Button-1>',clicked)
start_game()
root.mainloop() #не даёт закрыться программе

#button1=Button(root,text='ok',width=1,height=1,bg='black',fg='red',font='arial 24')
#перезапуск clicked DONE
#меню пауза Прямоугольник, текст, функции(tagbind), Сохранить, загрузить, рейтинг DONE
#сохранения отдельный файл в проекте, библиотека питона DONE
# МЕНЮ
# Звуки, музыка
#08.09.20 не появляется рестарт при самоубийстве DONE
#08.09.20 подсвечивать квадратик, об который произошла смерть DONE
#15.09.20 отображать текст на несколько секунд при сохранении DONE
#15.09.20 добавить нумерацию сейвов
#15.09.20 яблоко не должно появляться на змейке 50/50 (не видит s) DONE
#измена фона после загрузки DONE
#сохранять координаты каждого квадрата змейки, узнать длину с помощью len DONE
#17.09.20 сделать изменение цвета по формуле в зависимости от scoreNum (#112233) DONE
#22.09.20 рейтинг DONE
#функция, которая делает цвет квадрата в рейтинге противоположного цвета
#исправить цвет рейтинг DONE
#доработать рейтинг DONE
#добавить блоки, в которые змейка может врезаться 06.10.20 DONE
#игра ломается на сохранениях!!!!!!!!!!!!!!   DONE
#цвет не меняется при сохранении!!!!!!!!!!   DONE
#метод скрывать змейку и яблоко DONE
#если попал в рейтинг после смерти писать об этом DONE
#бомбы класс square DONE
#яблоко не появлялось на змейке DONE
# вернуть разрешение DONE
#заполнять массив рейтинга по порядку game_rating [75, 50, 0, 25, 25] DONE
#check_collision в классе бомб DONE
#08.09.20 добавить графику привязывая картинки DONE
####22.09.20 многопользовательский режим
#не появляются на змейке, на определенном расстоянии от головы, расстояние можно вычислять от скорости игры for i in range(len(s.square_list) DONE
#текст меню всегда поверх объектов DONE
###HARDCORE MODE когда бомбы появляются не от съеденного яблока, а по времени
#Бомба появляется за игровым полем DONE
#Не сохраняются бомбы и цвет DONE
## Генерация бомб рандом от обратного, нелинейный алгоритм для кол-ва бомб 1- log(1.2)
#реализовать плавное движение змейки ( не на 10 пикселей, а на 1)
#Картинка бомбы, DONE
# удаление картинки яблока рестар DONE
#возможность выбора начального цвета
#сделать save для between coords DONE
#проверка смерти в диапазоне DONE
#Яблоко появляется DONE
#Исправить Баг при загрузке, меню снизу отрисовывается ( заново отрисовывать меню ) DONE
#картинки бомб при загрузке DONE
#БАГ Хвост отделяется на секунду